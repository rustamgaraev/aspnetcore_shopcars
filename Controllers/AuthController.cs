﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using AspNetCore_ShopCars.Models;
using AspNetCore_ShopCars.Data;
using AspNetCore_ShopCars.ViewModels;
using System;

namespace AspNetCore_ShopCars.Controllers
{
    public class AuthController : Controller
    {
        private AppDBContent db;

        public AuthController(AppDBContent context)
        {
            db = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(model.Login); // аутентификация

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration(UserRegistrationModel model)
        {
            if (ValidateRegistration(model) && ModelState.IsValid)
            {
                User user = await db.Users.FirstOrDefaultAsync(u => u.Login == model.Login);
                if (user == null)
                {
                    // добавляем пользователя в бд
                    db.Users.Add(new User { ID = Guid.NewGuid(), Name = model.Name, Login = model.Login, Password = model.Password });
                    await db.SaveChangesAsync();

                    await Authenticate(model.Login); // аутентификация

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                }
            }

            return View(model);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };

            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Auth");
        }

        /// <summary>
        /// Валидация полей при регистрации.
        /// </summary>
        private bool ValidateRegistration(UserRegistrationModel model)
        {
            bool validate = true;

            if (model.Name == null)
            {
                ModelState.AddModelError("", "Укажите имя");
                validate = false;
            }

            if (model.Login == null)
            {
                ModelState.AddModelError("", "Укажите логин");
                validate = false;
            }

            if (model.Password == null)
            {
                ModelState.AddModelError("", "Укажите пароль");
                validate = false;
            }

            if (model.Password != null && model.ConfirmPassword == null)
            {
                ModelState.AddModelError("", "Повторите ввод пароля");
                validate = false;
            }

            if (model.Password != null && model.ConfirmPassword != null
                && model.Password != model.ConfirmPassword)
            {
                ModelState.AddModelError("", "Пароли должны совпадать");
                validate = false;
            }

            return validate;
        }
    }
}
