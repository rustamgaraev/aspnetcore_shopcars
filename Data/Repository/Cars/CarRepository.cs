﻿using AspNetCore_ShopCars.Data.Interfaces;
using AspNetCore_ShopCars.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCore_ShopCars.Data.Repository.Cars
{
    public class CarRepository : ICars
    {
        private readonly AppDBContent _appDBContent;

        public CarRepository(AppDBContent appDBContent)
        {
            _appDBContent = appDBContent;
        }

        public IEnumerable<Car> GetAllCars => _appDBContent.Car;

        public Car GetCarByID(Guid carID) => _appDBContent.Car.FirstOrDefault(x => x.ID == carID);

        public Car GetCarByName(string carName) => _appDBContent.Car.FirstOrDefault(x => x.Name == carName);
    }
}
