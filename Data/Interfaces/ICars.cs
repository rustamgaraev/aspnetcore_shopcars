﻿using AspNetCore_ShopCars.Data.Models;
using System;
using System.Collections.Generic;

namespace AspNetCore_ShopCars.Data.Interfaces
{
    public interface ICars
    {
        IEnumerable<Car> GetAllCars { get; }

        Car GetCarByID(Guid carID);

        Car GetCarByName(string carName);
    }
}
