﻿using AspNetCore_ShopCars.Data.Models;
using System.Collections.Generic;

namespace AspNetCore_ShopCars.Models
{
    public class CarListViewModel
    {
        public IEnumerable<Car> AllCars { get; set; }

        public Car Car { get; set; }
    }
}
