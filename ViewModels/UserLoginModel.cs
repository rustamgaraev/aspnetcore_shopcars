﻿using System.ComponentModel.DataAnnotations;

namespace AspNetCore_ShopCars.ViewModels
{
    public class UserLoginModel
    {
        [Required(ErrorMessage = "Не указан логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
