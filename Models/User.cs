﻿using System;

namespace AspNetCore_ShopCars.Models
{
    public class User
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}
