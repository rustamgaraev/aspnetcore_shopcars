﻿using AspNetCore_ShopCars.Data.Interfaces;
using AspNetCore_ShopCars.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCore_ShopCars.Controllers
{
    public class ShopController : Controller
    {
        private readonly ICars _cars;
        
        public ShopController(ICars cars)
        {
            _cars = cars;
        }

        [Authorize]
        public ViewResult ListCarsView()
        {
            ViewBag.Title = "Магазин";
            CarListViewModel obj = new CarListViewModel();
            obj.AllCars = _cars.GetAllCars;
            return View(obj);
        }
    }
}
