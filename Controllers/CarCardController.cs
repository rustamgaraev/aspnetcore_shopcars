﻿using AspNetCore_ShopCars.Data.Interfaces;
using AspNetCore_ShopCars.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace AspNetCore_ShopCars.Controllers
{
    public class CarCardController : Controller
    {
        private readonly ICars _cars;

        public CarCardController(ICars cars)
        {
            _cars = cars;
        }

        [Authorize]
        public IActionResult CarCardView(Guid id)
        {
            CarListViewModel obj = new CarListViewModel();
            obj.Car = _cars.GetCarByID(id);
            ViewBag.Title = obj.Car.Name;
            return View(obj);
        }
    }
}
