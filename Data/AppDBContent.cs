﻿using AspNetCore_ShopCars.Data.Models;
using AspNetCore_ShopCars.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetCore_ShopCars.Data
{
    public class AppDBContent : DbContext
    {
        public AppDBContent(DbContextOptions<AppDBContent> options) : base(options)
        {

        }

        // Автомобили
        public DbSet<Car> Car { get; set; }

        // Пользователи
        public DbSet<User> Users { get; set; }
    }
}
